<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'PassportController@login');
Route::post('register', 'PassportController@register');

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@details');
    Route::get('zuoraProducts', 'ZuoraController@zuoraProducts');
    Route::post('getToken', 'ZuoraController@getToken');
    Route::get('getAccountByKey', 'ZuoraController@getAccountByKey');

    Route::resource('products', 'ProductController');
});
