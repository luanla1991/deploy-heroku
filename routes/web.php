<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/nexmo', 'NexmoController@show')->name('nexmo');
//Route::post('/nexmo', 'NexmoController@verify')->name('nexmo');

//Route::get('user', function () {
//    return view('user', ['users' => App\User::all()]);
//});

Route::get('/verify', 'VerifyController@getVerify')->name('getVerify');
Route::post('/verify', 'VerifyController@postVerify')->name('verify');



Route::get('/send_email', array('uses' => 'EmailController@sendEmailReminder'));

Route::get('user', function () {
    return view('home');
});
