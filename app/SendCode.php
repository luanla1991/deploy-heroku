<?php

namespace App;

class SendCode
{
    public static function sendCode($phone){
        $code = rand(1111,9999);
        $nexmo = app('Nexmo\Client');
//        $nexmo->message()->send([
//            'to'   => '+84'. (int)$phone,
//            'from' => '+84816229049',
//            'text' => 'Đọc nhẹ giúp em cái code cái anh: ' . $code,
//        ]);

        $nexmo->message()->send([
            'to'   => '+84'. (int)$phone,
            'from' => '+84816229049',
            'text' => 'Verrify Code: ' . $code,
        ]);
        return $code;
    }
}

