<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ZuoraController extends Controller
{
    public function zuoraProducts(Request $request)
    {
        $zuoraUsername = env('ZUORA_USERNAME');
        $zuoraPassword = env('ZUORA_SECRET');
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://rest.apisandbox.zuora.com/v1/catalog/products');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


        $headers = array();
        $headers[] = 'Apiaccesskeyid:' . $zuoraUsername;
        $headers[] = 'Apisecretaccesskey:'. $zuoraPassword;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result;
//        return response()->json($result, 200);

//        return $this->response['response'] = json_decode($result);
    }

    public function getToken()
    {
        $zuoraClientId = env('ZUORA_CLIENT_ID');
        $zuoraClientSecret = env('ZUORA_CLIENT_SECRET');
        $data = [
           'client_id' =>  $zuoraClientId,
           'client_secret' =>  $zuoraClientSecret,
           'grant_type' =>  'client_credentials',
        ];
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://rest.apisandbox.zuora.com/oauth/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
//        curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=" . $zuoraClientId . "&client_secret=" .$zuoraClientSecret. "&grant_type=client_credentials");
//        curl_setopt($ch, CURLOPT_POSTFIELDS, "client_id=a1fedc05-2ebf-4d2d-9413-b8714efe31ab&client_secret=CjIxO3H6MCQeSTkG4kH/Cnz7+P4axZvc1aMhjjC8L&grant_type=client_credentials");

        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        $kq = json_decode($result, true);

//        return $result;
        echo $kq['access_token'];
    }

    public function getAccountByKey()
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://rest.apisandbox.zuora.com/v1/accounts/A00000198');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        $headers = array();
        $headers[] = 'Authorization: Bearer 3a0daada029842479bacc4c52067fa27';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
         return $result;
    }
}
