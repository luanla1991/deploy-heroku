<?php

namespace App\Http\Controllers;

use Mail;
use App\User;
use App\Mail\UserEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Mailgun\Mailgun;

class EmailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sendEmailReminder(Request $request)
    {
//        $id = 8; // điền 1 mã id bất kỳ của user trong bảng users
//        $user = User::findOrFail($id);
//
//        Mail::to($user)->send(new UserEmail());

//        $name = 'Luan La';
//        Mail::to('lathanhluan1991@gmail.com')->send(new UserEmail($name));
//
//        return 'Email was sent';


        $PRIVATE_API_KEY = env('MAILGUN_SECRET');
        $DOMAIN = env('MAILGUN_DOMAIN');


        //$mgClient = Mailgun::create('ac4aab0ac757361dd385e6c4d4483fc5-ed4dc7c4-763fbdb3');
        $mgClient = Mailgun::create($PRIVATE_API_KEY);
        //$domain = "sandbox2bd58b6b477e421fa356445e41b13044.mailgun.org";
        $domain = $DOMAIN;
# Make the call to the client.
        $result = $mgClient->messages()->send($domain, array(
            'from'	=> 'mailgun@gmail.com',
            'to'	=> 'lathanhluan1991@gmail.com',
//            'to'	=> 'taysonvo@gmail.com',
            'subject' => 'Hello',
            'text'	=> 'Check nhẹ giúp em cái email với anh SƠN!'
        ));

    }
}
